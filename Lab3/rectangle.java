
public class Rectangle extends Shape {
    private  double width;
    private double height;
 
  public Rectangle(double wid,double high){
      super(0,0);
   width=wid;
   height=high;
}
   public Rectangle(){
   super(0,0);
   height=1;
   width=1;
}
  public Rectangle(double x,double y,double wid,double high){
      super(x,y);
     
      width=wid;
     height=high;
     xPos=x;
     yPos=y;
  }
 public double getXPos(){
return xPos;
}
public void setXPos(double x){
xPos=x;
}
public double getYPos(){
return yPos;
}
    public void setYPos(double y){
yPos=y;
}
public double getWidth(){
return width;
}
public void setWidth(double wid){
width=wid;
}
public double getHeight(){
return height;
}
public void setHeight(double high){
height=high;
}
  public double getArea(){
  return width*height;
}
   @Override
    public String toString() {
        return ("Rectangle ( "+xPos + ", "+ yPos + " ) - ( " + (xPos+width) + ", " + (yPos + height) +" ).");
    }
  @Override
    public void printShape(){
        for(int i = 0; i < (int)height; i++){
            for(int j = 0; j < (int)width; j++){
                System.out.print("*");
            }
            System.out.println("");
        }
}
}

    