
public abstract class Shape {
   double xPos;
   double yPos;
   
   public Shape(double x,double y){
       xPos=x;
       yPos=y;
   }
   public Shape(){
       xPos=0;
       yPos=0;
   }
   public double getXPos(){
     return xPos;
}
    public void setXPos(double x){
    xPos=x;
}
   public double getYPos(){
      return yPos;
}
    public void setYPos(double y){
    yPos=y;
}
   @Override
    public String toString(){
return "Shape :+ ("+xPos+","+yPos+")";
}
   abstract public double getArea();
   abstract public void printShape(); 
}
        
    
