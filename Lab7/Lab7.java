public class FunctionalInterface {
   public static void main(String args[]){
       FunctionalInterface FunctionalInterface = new FunctionalInterface();

      MathOperation addition = (int a, int b) -> a + b;
		
     
      MathOperation subtraction = (a, b) -> a - b;
		
    
      MathOperation multiplication = (int a, int b) -> { return a * b; };
		
     
      MathOperation division = (int a, int b) -> a / b;
		
      System.out.println("27 + 7 = " + FunctionalInterface.operate(27,7, addition));
      System.out.println("35 - 4 = " + FunctionalInterface.operate(35, 4, subtraction));
      System.out.println("77 x 5 = " + FunctionalInterface.operate(77, 5, multiplication));
      System.out.println("355 / 5 = "+ FunctionalInterface.operate(355, 5, division));
		
   
      
   }
	
   interface MathOperation {
      int operation(int a, int b);
   }
	
  
	
   private int operate(int a, int b, MathOperation mathOperation){
      return mathOperation.operation(a, b);
   }
}