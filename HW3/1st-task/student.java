
 public class Student{
     private String name;
     private String major;
     private String email;
     private int nCredits;
     private Double gpa;
     public  String getName(){
         return name;
     }
             public void setName(String name){
                 this.name=name;
             }
            public  String getMajor(){ 
                return major;
            }
            public void setMajor(String major){
                this.major=major;
                
            }
            public String getEmail(){
                return email;
            }
            public void setEmail(String email){
                this.email=email;
            }
            public int getNCredits(){
                return nCredits;
            }
            public void setNCredits( int nCredits){
                this.nCredits=nCredits;
            }
            public double getGPA(){
                return gpa;
                
            }
            public void setGPA(double gpa){
                this.gpa=gpa;
            } 
            
            public void addTocredits(int c){
                this.nCredits=this.nCredits + c;
            }
 }