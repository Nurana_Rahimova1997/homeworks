public class Dog
{
 private int age;

 private String name; 

 public Dog(int age,String name)

 {
  this.age = age;

  this.name = name;
 } 

    
public String getName(){
    return name;
}
public void setName(String name){
    this.name=name;
}
public int getAge(){
    return age;
    
}
public void setAge(int age)
{
    this.age=age;
}
 public int computeDogAge()

 {

  return age * 7;

 }
 @Override
 public String toString()

 {

  String dogsname= "Dog's name: ";

  String dogsage= "Dog's age: ";

  return dogsname + name + "\t" + 

                  dogsage +  Integer.toString(age);

 } 
}